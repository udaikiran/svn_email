#!/bin/bash
set -x

patch_file=$1
DIR="/teamf1/dwc-2000/m2.4-new"

path=/home/udai/Documents/Learn/svn_email/$patch_file
patch -p4 --directory=$DIR < /home/udai/Documents/Learn/svn_email/$patch_file
files=`lsdiff /home/udai/Documents/Learn/svn_email/$patch_file | sed 's/m2.4/m2.4-new/g'`

filename=$(basename "$patch_file")
extension="${filename##*.}"
revision="${filename%.*}"

msg=`sqlite3 --line rev_log.db "select * from LOG_ENTRY where revision=$revision" | grep -v emailsent | tr '=' ':' `
cd $DIR

# /usr/bin/svn add $files
/usr/bin/svn commit $files -m "$msg"
echo $ret