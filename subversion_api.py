import pprint
import sqlite3
import svn.local
import os

from subprocess import call


class subversion:

    def __init__(self, svn_url, db_file_name):
        self._svn_url = svn_url
        self._db_file_name = db_file_name

        if os.path.isdir(svn_url) is False:
            raise AssertionError(svn_url + "does not exist")

        self._db_connect(db_file_name)
        self._svn_con = self._svn_connect(svn_url)
        self.svn_to_db_log_populate()

    def _db_connect(self, db_name):
        try:
            self._db_con = sqlite3.connect(
                db_name, detect_types=sqlite3.PARSE_DECLTYPES)
        except Exception as e:
            pprint.pprint(e)
            AssertionError("Unable to open db", db_name)
            exit("junk")

        cur = self._db_con.cursor()
        cur.execute("SELECT name FROM sqlite_master WHERE type='table';")
        for t in cur.fetchall():
            if t[0].decode('utf-8', "ignore") == "LOG_ENTRY":
                return self._db_con

        cur.executescript("""
            CREATE TABLE LOG_ENTRY(
                date string, commit_msg string, revision integer primary key, author string, emailsent integer);
            """)
        return self._db_con

    def db_log_msg_get(self, msg):
        if msg is not None:
            return msg.replace('"', '').replace('\'', '').strip()
        else:
            return ""

    def _db_log_entry_insert(self, log_entry):
        if self._db_con is None or log_entry is None:
            return None

        # check if the revision already exists.
        exec_stm = "select * from LOG_ENTRY where revision=%d" % log_entry.revision
        cur = self._db_con.cursor()
        rows = cur.execute(exec_stm).fetchall()

        if len(rows) != 0:
            return True

        exec_stm = "insert into LOG_ENTRY values ( '%s', '%s', %d, '%s', %d)" % (log_entry.date.strftime(
            "%Y-%m-%d %H:%M:%S"), self.db_log_msg_get(log_entry.msg), log_entry.revision, log_entry.author, 0)
        print("inserting revision", log_entry.revision)
        return self._db_con.cursor().execute(exec_stm)

    def _svn_connect(self, svn_url):
        try:
            self._svn_con = svn.local.LocalClient(svn_url)
        except Exception as e:
            pprint.pprint(e)
            return None

        return self._svn_con

    def svn_to_db_log_populate(self):
        return map(self._db_log_entry_insert, self._svn_con.log_default())

    def db_mark_log_email_sent(self):
        exec_stm = "UPDATE LOG_ENTRY SET emailsent=1"
        self._db_con.cursor().execute(exec_stm)
        return None

    def _db_all_revisions_get(self):
        exec_stm = "select revision from LOG_ENTRY"
        rows = self._db_con.cursor().execute(exec_stm).fetchall()
        revisions = list(map(lambda x: x[0], rows))
        revisions.sort()
        return revisions

    def svn_prev_version_get(self, revision):
        all_revisions = self._db_all_revisions_get()
        index = all_revisions.index(revision)
        return all_revisions[index-1]

    def db_revision_emails_not_sent_get(self):
        exec_stm1 = "select revision from LOG_ENTRY where emailsent=0"
        return list(map(lambda r: r[0], self._db_con.cursor().execute(exec_stm1).fetchall()))

    def author_get(self, revision):
        exec_stm = "select author from LOG_ENTRY where revision=%d" % revision
        rows = self._db_con.cursor().execute(exec_stm).fetchall()
        return rows[0][0]

    def db_revision_exists(self, revision):
        exec_stm = "select * from LOG_ENTRY where revision=%d" % revision
        rows = self._db_con.cursor().execute(exec_stm).fetchall()
        if len(rows) != 0:
            return True
        return False

    def db_last_email_sent_revision_get(self):
        exec_stm = "select revision from LOG_ENTRY where emailsent=1"
        return max(list(map(lambda r: r[0], self._db_con.cursor().execute(exec_stm).fetchall())))

    def changed_files_get(self, revision):
        prev_revision = self.svn_prev_version_get(revision)
        return self._svn_con.diff_summary(revision, prev_revision)

    def svn_diff_gen(self, revision):
        if self.db_revision_exists(revision) is False:
            return

        out = 'diffs/' + str(revision) + '.diff'
        touch_cmd = "/usr/bin/touch " + out
        diff_cmd = '/usr/bin/svn diff -c' + ' ' + \
            str(revision) + ' ' + self._svn_url + ' | filterdiff > ' + out
        os.system(touch_cmd)
        os.system(diff_cmd)

    def commit_msg_get(self, revision):
        exec_stm = "select commit_msg from LOG_ENTRY where revision=%d" % revision
        msg = self._db_con.cursor().execute(exec_stm).fetchall()
        return msg[0][0]
    
    def timestamp_get(self, revision):
        exec_stm = "select date from LOG_ENTRY where revision=%d" % revision
        msg = self._db_con.cursor().execute(exec_stm).fetchall()
        return msg[0][0]

    def update(self):
        return self._svn_con.update()

    def close(self):
        self._db_con.commit()
        self._db_con.close()

    def svn_commit(self):
        self._db_con.commit()


if __name__ == '__main__':
    url = '/teamf1/dwc-2000/m2.4'
    db_file_name = "rev_log.db"
    svn = subversion(url, db_file_name)
