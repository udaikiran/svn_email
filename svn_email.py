from subversion_api import subversion
from gmail_api import send_email
from datetime import datetime
import time
import pprint

url = '/teamf1/dwc-2000/m2.4/'
db_file_name = "rev_log.db"

def send_email_for_revision(revision):
    svn.svn_diff_gen(revision)
    author = svn.author_get(revision)
    to = "udaikiran@teamf1networks.com"
    subject = author + " comitted " + str(revision)
    attachment = 'diffs/' + str(revision) + '.diff'
    msg = "commit msg: " + svn.commit_msg_get(revision) + "<br/><br/>"
    msg = msg + "time:" + svn.timestamp_get(revision) + "<br/><br/>"
    cured_paths = map(lambda x:x['path'].replace(url,''), svn.changed_files_get(revision))
    msg = msg + "files changed: <br/>" + \
        reduce((lambda x, y: x + "<br/>" + y), cured_paths)
    send_email(to, subject, msg, attachment)
    pprint.pprint("sent email for revision %d" % revision)


if __name__ == '__main__':
    to = "udaikiran@teamf1networks.com"
    svn = subversion(url, db_file_name)

    while(True):
        svn.update()
        svn.svn_to_db_log_populate()
        map(send_email_for_revision, svn.db_revision_emails_not_sent_get())
        svn.db_mark_log_email_sent()
        svn.svn_commit()
        pprint.pprint(datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
        time.sleep(600)

    svn.close()
